'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'accounts',
      [
        {
          id: 'kande',
          name: 'Kaela & Elijah',
          details: 'Household Accounts',
          crap: 1000,
          groceries: 800,
          takeout: 500,
          income: 5248,
          expenses: 1894,
          createdAt: new Date('2020 02 15').toDateString(),
          updatedAt: new Date('2020 02 15').toDateString(),
        },
      ],
      {}
    )
  },

  down: (queryInterface, equelize) => {
    return queryInterface.bulkDelete('accounts', null, {})
  },
}
