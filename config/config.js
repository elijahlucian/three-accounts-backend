const dotenv = require('dotenv').config()

module.exports = {
  development: {
    use_env_variable: 'DATABASE_URL',
    dialect: 'postgres',
  },
  test: {
    dialect: 'sqlite',
    storage: './test.sqlite3',
  },
  production: {
    use_env_variable: 'DATABASE_URL',
    ssl: true,
    dialect: 'postgres',
    logging: false,
  },
}
