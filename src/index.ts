const express = require('express')
const dotenv = require('dotenv').config()
const socket = require('socket.io')
import * as path from 'path'
const moment = require('moment')
const Sequelize = require('sequelize')

class App {
  server: any
  db: any
  io: any

  constructor() {
    this.server = express()
    this.server.use(express.static(__dirname + '/../public'))
    this.server.use(express.urlencoded({ extended: true }))
    this.server.use(express.json())
    this.db = require('../models')
    this.io = socket()

    console.log(this.db ? '>> db connected!' : '>> db did not connect!!!')

    this.initSockets()
    this.init()
    this.server.get('*', (req, res) => {
      res.sendFile(path.join(__dirname, '/../public/index.html'))
    })
  }

  initSockets() {
    console.log('initializing sockets')
    this.io.on('connection', socket => {
      console.log('user connected', socket.id)
    })
  }

  init() {
    this.server.post(
      '/api/login',
      async ({ body: { email, password } }, res) => {
        if (!email || !password) {
          res.status(422)
          res.send({ message: 'you must supply a email and password!' })
          return
        }

        const record = await this.db.user.findOne({ where: { email } })

        if (record?.password === password) {
          console.log('user logged in', email)
          res.send(record)
        } else {
          res.status(403)
          res.send({ message: "Ain't the right password, dog!" })
        }
      }
    )

    // USERS
    this.server.get('/api/users/:id?', async ({ params: { id } }, res) => {
      const q = id
        ? await this.db.user.findOne({
            where: { id },
            include: ['accounts'],
          })
        : await this.db.user.findAll({ include: ['accounts'] })
      this.handleQuery(q, res)
    })

    this.server.post('/api/users', async ({ body }, res) => {
      console.log('CREATE USER =>', body.name)
      const id =
        body.name.replace(/\W/g, '') + Math.floor(Math.random() * 100000)
      const q = await this.db.user.create({ ...body, id })
      this.handleQuery(q, res)
    })

    this.server.put('/api/users/:id', async ({ params: { id }, body }, res) => {
      console.log('user updated', id, body)
      const record = await this.db.user.findOne({ where: { id } })
      const q = await record.update({ ...body })
      this.handleQuery(q, res)
    })

    // ACCOUNTS
    this.server.post(
      '/api/users/:id/accounts',
      async ({ params: { id }, body }, res) => {
        console.log('CREATE ACCOUNT =>', body.name)
        const user = await this.db.user.findOne({ where: { id } })
        const accountId = id + body.name.replace(/\W/g, '')
        const account = await user.createAccount({ ...body, id: accountId })

        this.handleQuery(account, res)
      }
    )
    this.server.get('/api/accounts/:id?', async ({ params: { id } }, res) => {
      const q = id
        ? await this.db.account.findOne({ where: { id }, include: ['users'] })
        : await this.db.account.findAll({ include: ['users'] })
      this.handleQuery(q, res)
    })
    this.server.put(
      '/api/accounts/:id',
      async ({ params: { id }, body }, res) => {
        console.log('account updated', id, body)
        const q = await this.db.account.update({ ...body }, { where: { id } })
        this.handleQuery(q, res)
        this.io.emit(id, 'refresh') // TODO: try dis
      }
    )
    this.server.post('/api/accounts', async ({ body }, res) => {
      const q = await this.db.account.create({ ...body })
      this.handleQuery(q, res)
    })

    this.server.get(
      '/api/accounts/:id/transactions/:year?/:month?',
      async ({ params: { id } }, res) => {
        const startOfMonth = moment()
          .startOf('month')
          .toDate()
        const endOfMonth = moment()
          .endOf('month')
          .toDate()
        const q = await this.db.transaction.findAll({
          where: {
            accountId: id,
            createdAt: { [Sequelize.Op.between]: [startOfMonth, endOfMonth] },
          },
        })
        res.send(q)
      }
    )

    this.server.post(
      '/api/accounts/:id/transactions',
      async ({ params: { id }, body }, res) => {
        console.log('transactions created for', id, body)
        this.db.transaction.create({ ...body, accountId: id })
        res.send({ body, id })
        this.io.emit(id, 'refresh')
      }
    )

    this.server.delete(
      '/api/accounts/:accountId/transactions/:id',
      async ({ params: { accountId, id } }, res) => {
        console.log('Delete', accountId, id)

        this.db.transaction.destroy({ where: { accountId, id } })
        res.send({ message: `transaction ${id} deleted` })
        this.io.emit(id, 'refresh')
      }
    )
  }

  handleQuery(record, res) {
    if (record) {
      res.send(record)
    } else this.notFound(res)
  }

  handleQueryAll(records, res) {
    if (records.length) {
      res.send(records)
    } else this.notFound(res)
  }

  notFound(res) {
    res.send({ message: 'not found' })
  }

  start() {
    const port = process.env.PORT
    this.server.listen(port)
    console.log(`listening on ${port}!`)
  }
}

const app = new App()
app.start()
