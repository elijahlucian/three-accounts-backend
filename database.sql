--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2 (Ubuntu 12.2-2.pgdg16.04+1)
-- Dumped by pg_dump version 12.2 (Ubuntu 12.2-2.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Account; Type: TABLE; Schema: public; Owner: accountable
--

CREATE TABLE public."Account" (
    id character varying(255) NOT NULL,
    name character varying(255),
    details character varying(255),
    groceries double precision,
    takeout double precision,
    crap double precision,
    income double precision,
    expenses double precision,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Account" OWNER TO accountable;

--
-- Name: SequelizeMeta; Type: TABLE; Schema: public; Owner: accountable
--

CREATE TABLE public."SequelizeMeta" (
    name character varying(255) NOT NULL
);


ALTER TABLE public."SequelizeMeta" OWNER TO accountable;

--
-- Name: Transaction; Type: TABLE; Schema: public; Owner: accountable
--

CREATE TABLE public."Transaction" (
    id integer NOT NULL,
    amount double precision,
    "accountName" character varying(255),
    "accountId" character varying(255),
    details character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Transaction" OWNER TO accountable;

--
-- Name: Transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: accountable
--

CREATE SEQUENCE public."Transaction_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Transaction_id_seq" OWNER TO accountable;

--
-- Name: Transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: accountable
--

ALTER SEQUENCE public."Transaction_id_seq" OWNED BY public."Transaction".id;


--
-- Name: User; Type: TABLE; Schema: public; Owner: accountable
--

CREATE TABLE public."User" (
    id character varying(255) NOT NULL,
    name character varying(255),
    email character varying(255),
    password character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."User" OWNER TO accountable;

--
-- Name: UserAccount; Type: TABLE; Schema: public; Owner: accountable
--

CREATE TABLE public."UserAccount" (
    "userId" character varying(255) NOT NULL,
    "accountId" character varying(255) NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."UserAccount" OWNER TO accountable;

--
-- Name: UserAccounts; Type: TABLE; Schema: public; Owner: accountable
--

CREATE TABLE public."UserAccounts" (
    "userId" character varying(255) NOT NULL,
    "accountId" character varying(255) NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."UserAccounts" OWNER TO accountable;

--
-- Name: accounts; Type: TABLE; Schema: public; Owner: accountable
--

CREATE TABLE public.accounts (
    id character varying(255) NOT NULL,
    name character varying(255),
    details character varying(255),
    groceries double precision,
    takeout double precision,
    crap double precision,
    income double precision,
    expenses double precision,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.accounts OWNER TO accountable;

--
-- Name: transactions; Type: TABLE; Schema: public; Owner: accountable
--

CREATE TABLE public.transactions (
    id integer NOT NULL,
    amount double precision,
    "accountName" character varying(255),
    "accountId" character varying(255),
    details character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.transactions OWNER TO accountable;

--
-- Name: transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: accountable
--

CREATE SEQUENCE public.transactions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transactions_id_seq OWNER TO accountable;

--
-- Name: transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: accountable
--

ALTER SEQUENCE public.transactions_id_seq OWNED BY public.transactions.id;


--
-- Name: user_accounts; Type: TABLE; Schema: public; Owner: accountable
--

CREATE TABLE public.user_accounts (
    "userId" character varying(255) NOT NULL,
    "accountId" character varying(255) NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.user_accounts OWNER TO accountable;

--
-- Name: users; Type: TABLE; Schema: public; Owner: accountable
--

CREATE TABLE public.users (
    id character varying(255) NOT NULL,
    name character varying(255),
    email character varying(255),
    password character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.users OWNER TO accountable;

--
-- Name: Transaction id; Type: DEFAULT; Schema: public; Owner: accountable
--

ALTER TABLE ONLY public."Transaction" ALTER COLUMN id SET DEFAULT nextval('public."Transaction_id_seq"'::regclass);


--
-- Name: transactions id; Type: DEFAULT; Schema: public; Owner: accountable
--

ALTER TABLE ONLY public.transactions ALTER COLUMN id SET DEFAULT nextval('public.transactions_id_seq'::regclass);


--
-- Data for Name: Account; Type: TABLE DATA; Schema: public; Owner: accountable
--

COPY public."Account" (id, name, details, groceries, takeout, crap, income, expenses, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: accountable
--

COPY public."SequelizeMeta" (name) FROM stdin;
20200226055853-create-transaction.js
20200226055953-create-account.js
20200226060155-create-user.js
20200229023254-associate-user-account.js
\.


--
-- Data for Name: Transaction; Type: TABLE DATA; Schema: public; Owner: accountable
--

COPY public."Transaction" (id, amount, "accountName", "accountId", details, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: User; Type: TABLE DATA; Schema: public; Owner: accountable
--

COPY public."User" (id, name, email, password, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: UserAccount; Type: TABLE DATA; Schema: public; Owner: accountable
--

COPY public."UserAccount" ("userId", "accountId", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: UserAccounts; Type: TABLE DATA; Schema: public; Owner: accountable
--

COPY public."UserAccounts" ("userId", "accountId", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for Name: accounts; Type: TABLE DATA; Schema: public; Owner: accountable
--

COPY public.accounts (id, name, details, groceries, takeout, crap, income, expenses, "createdAt", "updatedAt") FROM stdin;
kande	Kaela & Elijah	Household Accounts	800	500	1000	5248	1894	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
Nick51697Nick	Nick	\N	400	400	500	\N	\N	2020-03-01 17:29:34.568+00	2020-03-01 17:29:34.568+00
Nick70744Nick	Nick	\N	400	400	400	\N	\N	2020-03-01 17:44:53.73+00	2020-03-01 17:44:53.73+00
Nicholas4527Monthly	Monthly	Balls deep	400	400	400	3200	1200	2020-03-01 17:51:13.173+00	2020-03-01 17:51:13.173+00
Nick67618Nick	Nick	Ballsdeepkentucky	400	400	400	3200	1200	2020-03-01 18:24:23.531+00	2020-03-01 18:24:23.531+00
asdf123469248personal	personal	blah	100	100	100	5000	2000	2020-03-01 18:28:22.803+00	2020-03-01 18:28:22.803+00
Nick32802Nicksbudget	Nick's budget	Deez nutz	400	400	400	3200	1400	2020-03-01 19:27:20.996+00	2020-03-01 19:27:20.996+00
elijahtestbudget	test budget	for testing transaction	500	250	1000	4000	200	2020-03-01 19:33:46.615+00	2020-03-01 19:33:46.615+00
Nickpackerswingindiksgov19272Nicksbudget	Nick's budget	Big shit	400	400	400	3200	1200	2020-03-01 20:01:47.091+00	2020-03-01 20:01:47.091+00
Nickpackerswingindiksgov19272Hugedicks	Huge dicks	Fuck	500	600	7800	2	5000	2020-03-01 20:03:16.353+00	2020-03-01 20:03:16.353+00
warao68427Weeklybudget	Weekly budget	\N	\N	\N	100	200	\N	2020-04-03 00:06:12.545+00	2020-04-03 00:06:12.545+00
\.


--
-- Data for Name: transactions; Type: TABLE DATA; Schema: public; Owner: accountable
--

COPY public.transactions (id, amount, "accountName", "accountId", details, "createdAt", "updatedAt") FROM stdin;
48	400	groceries	kande	halfmonth	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
49	122	groceries	kande	superstore	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
50	21.71	groceries	kande	community	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
51	75.08	groceries	kande	sobeys	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
52	32.56	groceries	kande	sobeys	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
53	500	crap	kande	halfmonth	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
54	149.09	crap	kande	vechicle	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
55	20	crap	kande	starbuckasfadfas	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
56	20.81	crap	kande	thriftstore	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
57	47	crap	kande	gas raaaav4	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
58	40	crap	kande	gas vaaaaan	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
59	250	takeout	kande	halfmonth	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
60	47.12	takeout	kande	raj palace	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
61	14.66	takeout	kande	afewngkagnewlakg	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
62	18.48	takeout	kande	safari grill	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
63	72.82	takeout	kande	Li Ao	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
64	13.58	takeout	kande	Thai Express	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
65	25	crap	kande	Parking	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
66	90	crap	kande	Eyelashes	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
67	46	groceries	kande	Sobeys	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
68	66	groceries	kande	Sobeys	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
69	75	crap	kande	Vaugn Bday	2020-02-25 00:00:00+00	2020-02-25 00:00:00+00
70	20	takeout	kande	DQ	2020-02-28 00:00:00+00	2020-02-28 00:00:00+00
71	23	takeout	kande	Raj	2020-02-28 00:00:00+00	2020-02-28 00:00:00+00
72	40	crap	Nick32802Nicksbudget	Starbucks	2020-03-01 19:28:03.041+00	2020-03-01 19:28:03.041+00
73	300	groceries	Nick32802Nicksbudget	Grocery trip Feb 22	2020-03-01 19:28:19.805+00	2020-03-01 19:28:19.805+00
74	500	groceries	Nick32802Nicksbudget	Groceries fuck	2020-03-01 19:28:31.029+00	2020-03-01 19:28:31.029+00
75	400	takeout	Nick32802Nicksbudget	Ate a lot	2020-03-01 19:28:56.345+00	2020-03-01 19:28:56.345+00
76	50	takeout	elijahtestbudget	something	2020-03-01 19:33:51.836+00	2020-03-01 19:33:51.836+00
77	500	crap	elijahtestbudget	a gun	2020-03-01 19:33:59.225+00	2020-03-01 19:33:59.225+00
78	200	groceries	elijahtestbudget	sobeys	2020-03-01 19:34:04.21+00	2020-03-01 19:34:04.21+00
79	40	groceries	Nickpackerswingindiksgov19272Nicksbudget	Grocery trip	2020-03-01 20:02:05.1+00	2020-03-01 20:02:05.1+00
80	400	takeout	Nickpackerswingindiksgov19272Nicksbudget	I'm a pig	2020-03-01 20:02:21.385+00	2020-03-01 20:02:21.385+00
82	55.49	takeout	kande	Dave's bday	2020-03-02 02:17:55.146+00	2020-03-02 02:17:55.146+00
83	63.82	crap	kande	WEED	2020-03-02 03:00:58.148+00	2020-03-02 03:00:58.148+00
84	20	groceries	kande	Sobeys	2020-03-03 15:25:11.613+00	2020-03-03 15:25:11.613+00
86	20	takeout	kande	Coffee with mel	2020-03-05 00:05:04.204+00	2020-03-05 00:05:04.204+00
87	16	groceries	kande	sobeys - salad - creamer - avocado	2020-03-05 03:00:01.456+00	2020-03-05 03:00:01.456+00
88	24.38	groceries	kande	Sobeys	2020-03-05 15:14:12.772+00	2020-03-05 15:14:12.772+00
89	53	groceries	kande	Sobeys	2020-03-05 22:11:15.869+00	2020-03-05 22:11:15.869+00
122	12	takeout	kande	Marvs	2020-03-06 19:28:08.707+00	2020-03-06 19:28:08.707+00
123	11.54	takeout	kande	Jerusalem	2020-03-06 19:42:49.766+00	2020-03-06 19:42:49.766+00
125	6.18	takeout	kande	fratello cafe	2020-03-07 15:25:11.39+00	2020-03-07 15:25:11.39+00
126	111.44	crap	kande		2020-03-07 15:25:41.467+00	2020-03-07 15:25:41.467+00
127	30.37	takeout	kande	lunch w/ dawin	2020-03-07 15:27:23.535+00	2020-03-07 15:27:23.535+00
128	43.38	groceries	kande	Community	2020-03-07 21:16:14.781+00	2020-03-07 21:16:14.781+00
129	14.64	takeout	kande	Banzai	2020-03-07 21:32:16.745+00	2020-03-07 21:32:16.745+00
130	38.83	crap	kande	Allergy shit	2020-03-07 21:38:21.624+00	2020-03-07 21:38:21.624+00
131	162.74	crap	kande	Mixer l&m	2020-03-07 22:49:12.032+00	2020-03-07 22:49:12.032+00
132	23	groceries	kande	Sausages	2020-03-07 23:22:28.471+00	2020-03-07 23:22:28.471+00
133	3	crap	kande	Coffee	2020-03-07 23:23:08.795+00	2020-03-07 23:23:08.795+00
134	44	crap	kande	Balsamic and vinegar	2020-03-07 23:36:05.802+00	2020-03-07 23:36:05.802+00
135	50.6	groceries	kande	Bulk barn	2020-03-08 21:59:45.87+00	2020-03-08 21:59:45.87+00
136	31	crap	kande	Toffee haircut	2020-03-11 14:54:51.927+00	2020-03-11 14:54:51.927+00
137	23	groceries	kande	Sobeys	2020-03-11 14:55:10.051+00	2020-03-11 14:55:10.051+00
138	23	takeout	kande	Tea with mandy	2020-03-11 17:03:04.67+00	2020-03-11 17:03:04.67+00
139	63.91	groceries	kande	Sobeys	2020-03-12 00:33:09.601+00	2020-03-12 00:33:09.601+00
140	23	takeout	kande	Bennys	2020-03-13 02:05:38.898+00	2020-03-13 02:05:38.898+00
141	20	groceries	kande	Sobeys	2020-03-14 00:57:31.468+00	2020-03-14 00:57:31.468+00
142	24	takeout	kande	Skip	2020-03-15 01:49:05.16+00	2020-03-15 01:49:05.16+00
143	60	crap	kande	Karaoke	2020-03-15 06:48:18.499+00	2020-03-15 06:48:18.499+00
144	13.71	crap	kande	Plungers	2020-03-16 21:43:40.328+00	2020-03-16 21:43:40.328+00
145	56.89	groceries	kande	Grocs	2020-03-16 21:44:04.893+00	2020-03-16 21:44:04.893+00
146	8	crap	kande	Sally’s 	2020-03-17 02:31:48.323+00	2020-03-17 02:31:48.323+00
147	16	crap	kande	Bakery	2020-03-17 02:31:57.14+00	2020-03-17 02:31:57.14+00
148	153	crap	kande	Bulk barn	2020-03-17 21:45:07.757+00	2020-03-17 21:45:07.757+00
149	376.91	groceries	kande	Superstore	2020-03-17 21:45:44.698+00	2020-03-17 21:45:44.698+00
150	43.84	groceries	kande	Superstore	2020-03-17 21:46:19.703+00	2020-03-17 21:46:19.703+00
151	26	takeout	kande	Taco time	2020-03-17 21:48:21.491+00	2020-03-17 21:48:21.491+00
152	141	crap	kande	BOOZE	2020-03-17 22:12:28.397+00	2020-03-17 22:12:28.397+00
153	21	crap	kande	far cry: new dawn	2020-03-18 23:29:22.192+00	2020-03-18 23:29:22.192+00
154	30	crap	kande	Groceries 	2020-03-19 20:46:39.679+00	2020-03-19 20:46:39.679+00
155	500	crap	elijahtestbudget	some random thing	2020-04-03 00:05:47.22+00	2020-04-03 00:05:47.22+00
156	25	takeout	elijahtestbudget	mcdonalds	2020-04-03 00:05:53.804+00	2020-04-03 00:05:53.804+00
157	15	crap	warao68427Weeklybudget	beers	2020-04-03 00:07:48.241+00	2020-04-03 00:07:48.241+00
158	60.23	groceries	kande	Superstore	2020-04-03 01:21:27.846+00	2020-04-03 01:21:27.846+00
191	178.49	crap	kande	Elijah Keybaord	2020-04-03 18:57:28.138+00	2020-04-03 18:57:28.138+00
192	28	takeout	kande	Li	2020-04-03 23:11:01.8+00	2020-04-03 23:11:01.8+00
193	150	crap	kande	Elijah - tascam	2020-04-04 19:04:46.075+00	2020-04-04 19:04:46.075+00
194	52.5	crap	kande	Elijah - Coffee Grinde	2020-04-05 16:40:21.356+00	2020-04-05 16:40:21.356+00
195	49.35	groceries	kande	Amazon - Coffee And Hot Chocoalte	2020-04-05 16:41:00.532+00	2020-04-05 16:41:00.532+00
196	9.2	crap	kande	amazon - plug for recorder	2020-04-05 16:41:28.948+00	2020-04-05 16:41:28.948+00
197	131	groceries	kande	Sobeys	2020-04-24 00:10:46.095+00	2020-04-24 00:10:46.095+00
\.


--
-- Data for Name: user_accounts; Type: TABLE DATA; Schema: public; Owner: accountable
--

COPY public.user_accounts ("userId", "accountId", "createdAt", "updatedAt") FROM stdin;
elijah	kande	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
kaela	kande	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
Nick51697	Nick51697Nick	2020-03-01 17:29:34.583+00	2020-03-01 17:29:34.583+00
Nick70744	Nick70744Nick	2020-03-01 17:44:53.739+00	2020-03-01 17:44:53.739+00
Nicholas4527	Nicholas4527Monthly	2020-03-01 17:51:13.184+00	2020-03-01 17:51:13.184+00
Nick67618	Nick67618Nick	2020-03-01 18:24:23.561+00	2020-03-01 18:24:23.561+00
asdf123469248	asdf123469248personal	2020-03-01 18:28:22.825+00	2020-03-01 18:28:22.825+00
Nick32802	Nick32802Nicksbudget	2020-03-01 19:27:21.007+00	2020-03-01 19:27:21.007+00
elijah	elijahtestbudget	2020-03-01 19:33:46.624+00	2020-03-01 19:33:46.624+00
Nickpackerswingindiksgov19272	Nickpackerswingindiksgov19272Nicksbudget	2020-03-01 20:01:47.099+00	2020-03-01 20:01:47.099+00
Nickpackerswingindiksgov19272	Nickpackerswingindiksgov19272Hugedicks	2020-03-01 20:03:16.361+00	2020-03-01 20:03:16.361+00
warao68427	warao68427Weeklybudget	2020-04-03 00:06:12.561+00	2020-04-03 00:06:12.561+00
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: accountable
--

COPY public.users (id, name, email, password, "createdAt", "updatedAt") FROM stdin;
elijah	elijah	elijahlucian@gmail.com	toffee15	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
kaela	kaela	kaelacaron@gmail.com	wicked	2020-02-15 00:00:00+00	2020-02-15 00:00:00+00
Nick51697	Nick	nicholasowenpacker@gmail.com	Nickisacooldude1	2020-03-01 17:29:14.617+00	2020-03-01 17:29:14.617+00
Emoney77961	Emoney	emoney@emoney.com	emoney	2020-03-01 17:43:25.705+00	2020-03-01 17:43:25.705+00
Nick70744	Nick	nicholasowenpacker@gmail.com	Nickisacooldude1	2020-03-01 17:44:39.663+00	2020-03-01 17:44:39.663+00
Nicholas4527	Nicholas 	nicholasowenpacker@gmail.com	Nickisacooldude1	2020-03-01 17:50:40.94+00	2020-03-01 17:50:40.94+00
Nick28258	Nick	Nickpacker@swingindiks.gov	bigshoots	2020-03-01 17:54:07.498+00	2020-03-01 17:54:07.498+00
Nick67618	Nick	bigdick@gmail.com	rickstick	2020-03-01 18:24:01.041+00	2020-03-01 18:24:01.041+00
asdf123469248	asdf1234	asdf1234@asdf.com	asdf1234	2020-03-01 18:28:02.055+00	2020-03-01 18:28:02.055+00
Nick32802	Nick	bigstink@fuck.com	realbig	2020-03-01 19:26:56.49+00	2020-03-01 19:26:56.49+00
Nickpackerswingindiksgov19272	Nickpacker@swingindiks.gov 	Nickpacker@swingindiks.gov	cooldude	2020-03-01 20:01:02.439+00	2020-03-01 20:01:02.439+00
warao68427	warao	warao@test.com	123456	2020-04-03 00:05:45.533+00	2020-04-03 00:05:45.533+00
\.


--
-- Name: Transaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: accountable
--

SELECT pg_catalog.setval('public."Transaction_id_seq"', 1, false);


--
-- Name: transactions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: accountable
--

SELECT pg_catalog.setval('public.transactions_id_seq', 197, true);


--
-- Name: Account Account_pkey; Type: CONSTRAINT; Schema: public; Owner: accountable
--

ALTER TABLE ONLY public."Account"
    ADD CONSTRAINT "Account_pkey" PRIMARY KEY (id);


--
-- Name: SequelizeMeta SequelizeMeta_pkey; Type: CONSTRAINT; Schema: public; Owner: accountable
--

ALTER TABLE ONLY public."SequelizeMeta"
    ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY (name);


--
-- Name: Transaction Transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: accountable
--

ALTER TABLE ONLY public."Transaction"
    ADD CONSTRAINT "Transaction_pkey" PRIMARY KEY (id);


--
-- Name: UserAccount UserAccount_pkey; Type: CONSTRAINT; Schema: public; Owner: accountable
--

ALTER TABLE ONLY public."UserAccount"
    ADD CONSTRAINT "UserAccount_pkey" PRIMARY KEY ("userId", "accountId");


--
-- Name: UserAccounts UserAccounts_pkey; Type: CONSTRAINT; Schema: public; Owner: accountable
--

ALTER TABLE ONLY public."UserAccounts"
    ADD CONSTRAINT "UserAccounts_pkey" PRIMARY KEY ("userId", "accountId");


--
-- Name: User User_pkey; Type: CONSTRAINT; Schema: public; Owner: accountable
--

ALTER TABLE ONLY public."User"
    ADD CONSTRAINT "User_pkey" PRIMARY KEY (id);


--
-- Name: accounts accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: accountable
--

ALTER TABLE ONLY public.accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (id);


--
-- Name: transactions transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: accountable
--

ALTER TABLE ONLY public.transactions
    ADD CONSTRAINT transactions_pkey PRIMARY KEY (id);


--
-- Name: user_accounts user_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: accountable
--

ALTER TABLE ONLY public.user_accounts
    ADD CONSTRAINT user_accounts_pkey PRIMARY KEY ("userId", "accountId");


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: accountable
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: accountable
--

REVOKE ALL ON SCHEMA public FROM postgres;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO accountable;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: LANGUAGE plpgsql; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON LANGUAGE plpgsql TO accountable;


--
-- PostgreSQL database dump complete
--

