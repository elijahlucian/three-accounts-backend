'use strict'
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'user',
    {
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {}
  )

  User.associate = ({ account }) => {
    User.belongsToMany(account, {
      through: 'user_accounts',
      as: 'accounts',
      foreignKey: 'userId',
    })
    // User.hasMany(models.accounts)
    // associations can be defined here
  }

  return User
}
